public class URL {
    private String domain; //no need to set final since there is no set function
    private String protocol;
    private String path;

    public URL(String domain, String protocol, String path) {
        this.domain = domain;
        this.protocol = protocol;
        this.path = path;
    }

    public String getDomain() {
        if (domain.trim().length()==0) throw new IllegalArgumentException("The domain cannot be emptied.");

        return domain;
    }

    public String getProtocol(){
        boolean flag = false;

        //protocol cannot be emptied
        if (protocol.trim().length()==0) throw new IllegalArgumentException("The protocol cannot be emptied.");

        //protocol can be only in ftp, http or https.
        if (protocol.equals("ftp") || protocol.equals("http") || protocol.equals("https") ){
            flag = true;
        }

        if (!flag) throw new IllegalArgumentException("The protocol is incorrect!");

        return protocol;
    }

    public String getPath() {
        return path;
    }
    public String toString() {
        return getProtocol()+"://" + getDomain() +"/"+getPath();
    }

}
