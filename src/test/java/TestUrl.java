
//import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class TestUrl {

    @Test
    public void testRomanNumeral(){
        URL url = new URL("launchcode.org","https","learn");

        assertEquals("launchcode.org",  url.getDomain() );
        assertEquals("https://launchcode.org/learn",  url.toString() );
    }
    @Test
    //@Test(expected = IllegalArgumentException.class)
    public void testURL(){
        try{
            String protocol = "https";
            URL url = new URL("launchcode.org", protocol,"learn");

            url.getProtocol();
            url.getDomain();
            assertEquals(protocol,  url.getProtocol() );
            //fail("Incorrect formats! Please check names of the protocol and domain.");//fail because it's of JUnit older than 4.12

            //below two lines, we only need @Test and no need for @Test(expect = IllegalArgumentException.class)
            //exception.expect(IllegalArgumentException.class);
           // exception.expectMessage("Incorrect formats! Please check names of the protocol and domain.");


        }catch (IllegalArgumentException e){
            System.out.println("Error found: " + e.getMessage());
        }

    }

    @Rule
    public ExpectedException exception = ExpectedException.none();


}
